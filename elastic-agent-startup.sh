#!/bin/bash

echo "==========================================="
echo "============= STARTUP SCRIPT =============="
echo "==========================================="

aws configure set aws_access_key_id $1
aws configure set aws_secret_access_key $2
aws configure set default.region $3

export INSTANCE_ID=`curl -s http://169.254.169.254/latest/meta-data/instance-id`

echo "Tagging this instance..."
echo $INSTANCE_ID

tags=
while IFS=, read -r key value
do
    tags=$tags" Key=$key,Value=$value"
done < $4

# Base tags
aws ec2 create-tags --resources $INSTANCE_ID --tags $tags

# Additional tags
aws ec2 create-tags --resources $INSTANCE_ID --tags $5

echo "==========================================="